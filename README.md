# Homes Angular App

This is a demo Angular application for managing housing locations. The app demonstrates frontend-backend communication using Docker and provides a live preview hosted on Render. It supports two environments: development and production.

## Live Demo

Check out the live application hosted on [Render](https://homes-angular-app.onrender.com).  

⚠️**Note:** The application is hosted on a free-tier environment, which may cause a 30-40 second delay during the first load after inactivity. Both the frontend and backend take time to start, with the frontend appearing first while the backend initializes. This happens only on the initial load and is not an error.

## Features

- **Angular Frontend:** Modern client-side application with routing and form validation.
- **Mock Backend:** Simulated API using JSON Server to manage housing data.
- **Dockerized Setup:** Fully containerized environment with Docker Compose to run both frontend and backend services.
- **Nginx Configuration:** Serves Angular's static files and supports client-side routing.
- **Validation and Feedback:** Dynamic form validation and API responses based on housing availability.

## Technologies

- **Frontend:** Angular, TypeScript
- **Backend:** JSON Server (mock backend)
- **Deployment:** Render (for hosting)
- **Docker and Docker Compose:** Used for containerizing both frontend and backend
- **Nginx** used for serving static files and routing.


## Project Structure

```plaintext
├── api/                   # Contains the JSON mock backend
│   ├── db.json            # Mock database file
│   ├── Dockerfile         # Dockerfile for the backend
│   ├── package.json        
|
├── src/                   # Angular application source code
├── Dockerfile             # Dockerfile for the frontend
├── docker-compose.yml     # Configuration for running frontend and backend
├── nginx.config           # Nginx configuration for serving the Angular app
```

## Prerequisites

- **Node.js** (v14.x or higher)
- **Docker** (v20.x or higher)
- **Angular CLI** (v15.x or higher, optional for local development)

## Local Development

### Step 1: Run the Mock Backend

- Start the JSON Server to simulate the API:

```bash
cd api
json-server --watch db.json
```

- Verify the backend is running at http://localhost:3000/locations.

### Step 2: Run the Angular Application

- Navigate to the Angular project directory and start the development server:

```bash
ng serve
```
- Open http://localhost:4200 in your browser.
- The application will interact with the mock API hosted at http://localhost:3000.

## Build and Deployment
### Build for Production
- To build the Angular application for production:

```bash
ng build --configuration production
```
- The build files will be located in the dist/my-first-project directory.

### Production API
In production mode, the application interacts with the live API at:
https://homes-angular-app-backend.onrender.com/locations.
This is automatically configured through Angular's production settings.

### Dockerized Deployment
- The project is preconfigured to run in a Dockerized environment with Docker Compose.

**Step 1:** Build and Run with Docker Compose

```bash
docker-compose up --build
```
**Step 2:** Access the Application

- **Frontend:** http://localhost
- **Backend (JSON API):** http://localhost:3000/locations

## Deployment with Nginx
Nginx is used to serve the Angular application and handle client-side routing. The nginx.config file defines the server configuration, including fallback routing for Angular.

## Testing

### Local Testing
1. **API Requests:** Use the browser's Developer Tools (Network tab) to inspect API requests.

- Development Mode:** Requests should go to http://localhost:3000/locations.
- Production Mode: Requests should go to https://homes-angular-app-backend.onrender.com/locations.

2. **Form Validation:** Test the form submission with valid and invalid data.

3. **Edge Cases:** Ensure appropriate responses for housing applications when no units are available.

### Production Testing
Access the live application on [Render](https://homes-angular-app.onrender.com) to verify the deployed version.



## Known Limitations
- **Free-tier Hosting:** Render's free-tier hosting might cause delays during the initial load.
- **Mock Backend:** The JSON Server is a placeholder and not suitable for production use.





