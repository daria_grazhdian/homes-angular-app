import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { HousingService } from '../housing.service';
import { HousingLocation } from '../housinglocation';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-details',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  templateUrl: 'details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent {
  route: ActivatedRoute = inject(ActivatedRoute);
  housingService = inject(HousingService);
  housingLocation: HousingLocation | undefined;

  applyForm = new FormGroup({
    firstName: new FormControl('', [Validators.required, Validators.minLength(2)]),
    lastName: new FormControl('', [Validators.required, Validators.minLength(2)]),
    email: new FormControl('', [Validators.required, Validators.email]),
  });

  constructor() {
    const housingLocationId = parseInt(this.route.snapshot.params['id'], 10);
    this.housingService
      .getHousingLocationById(housingLocationId)
      .subscribe((housingLocation) => {
        this.housingLocation = housingLocation;
      });
  }

  submitApplication() {
    if (this.housingLocation?.availableUnits === 0) {
      alert('Unfortunately, no units are available at this location.');
      return;
    }

    if (this.applyForm.invalid) {
      alert('Please fill out the form correctly before submitting.');
      return; 
    }
  
    if (this.housingLocation) {
      alert('Your application has been successfully submitted!');
      this.housingLocation.availableUnits--; 
    } else {
      alert('Error: Housing location data is not available.');
    }
  }
  
  
}



