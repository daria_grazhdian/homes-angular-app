export const environment = {
    production: true,
    apiUrl: 'https://homes-angular-app-backend.onrender.com/locations',
  };
  